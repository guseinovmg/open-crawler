'use strict';

var parsing = require('../parsing.js');

module.exports = [
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'kkb.kz',
            url: 'http://kkb.kz',
            selectors: {
                usd_buy: '#div_sub1  table  tbody:nth-child(2)  tr:nth-child(1) > td:nth-child(2)',
                usd_sell: '#div_sub1  table  tbody:nth-child(2)  tr:nth-child(1) > td:nth-child(3)',
                eur_buy: '#div_sub1  table  tbody:nth-child(2)  tr:nth-child(2) > td:nth-child(2)',
                eur_sell: '#div_sub1  table  tbody:nth-child(2)  tr:nth-child(2) > td:nth-child(3)',
                rub_buy: '#div_sub1  table  tbody:nth-child(2)  tr:nth-child(3) > td:nth-child(2)',
                rub_sell: '#div_sub1  table  tbody:nth-child(2)  tr:nth-child(3) > td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'halykbank.kz',
            url: 'http://www.halykbank.kz/',
            selectors: {
                usd_buy: '#home  div.home-right  div.rates  table.rates-1.js-rates-1  tr:nth-child(2) > td:nth-child(2)',
                usd_sell: '#home  div.home-right  div.rates  table.rates-1.js-rates-1  tr:nth-child(2) > td:nth-child(3)',
                eur_buy: '#home  div.home-right  div.rates  table.rates-1.js-rates-1  tr:nth-child(3) > td:nth-child(2)',
                eur_sell: '#home  div.home-right  div.rates  table.rates-1.js-rates-1 tr:nth-child(3) > td:nth-child(3)',
                rub_buy: '#home  div.home-right  div.rates  table.rates-1.js-rates-1  tr:nth-child(4) > td:nth-child(2)',
                rub_sell: '#home  div.home-right  div.rates  table.rates-1.js-rates-1  tr:nth-child(4) > td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'bcc.kz',
            url: 'http://www.bcc.kz/',
            selectors: {
                usd_buy: 'body div.bcc_kurs.ifleft > div.bcc_kurs_items > div:nth-child(3) > div:nth-child(2)',
                usd_sell: 'body div.bcc_kurs.ifleft > div.bcc_kurs_items > div:nth-child(3) > div:nth-child(3)',
                eur_buy: 'body div.bcc_kurs.ifleft > div.bcc_kurs_items > div:nth-child(4) > div:nth-child(2)',
                eur_sell: 'body div.bcc_kurs.ifleft > div.bcc_kurs_items > div:nth-child(4) > div:nth-child(3)',
                rub_buy: 'body div.bcc_kurs.ifleft > div.bcc_kurs_items > div:nth-child(5) > div:nth-child(2)',
                rub_sell: 'body div.bcc_kurs.ifleft > div.bcc_kurs_items > div:nth-child(5) > div:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'eubank.kz',
            url: 'http://eubank.kz/',
            selectors: {
                usd_buy: 'body  div.wraper  div.exchange  table:nth-child(6)  tr:nth-child(2) > td:nth-child(2)',
                usd_sell: 'body  div.wraper  div.exchange  table:nth-child(6)  tr:nth-child(2) > td:nth-child(3)',
                eur_buy: 'body  div.wraper  div.exchange  table:nth-child(6)  tr:nth-child(3) > td:nth-child(2)',
                eur_sell: 'body  div.wraper  div.exchange  table:nth-child(6)  tr:nth-child(3) > td:nth-child(3)',
                rub_buy: 'body  div.wraper  div.exchange  table:nth-child(6)  tr:nth-child(4) > td:nth-child(2)',
                rub_sell: 'body  div.wraper  div.exchange  table:nth-child(6)  tr:nth-child(4) > td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'nurbank.kz',
            url: 'http://nurbank.kz/currency',
            selectors: {
                usd_buy: '#data2  table tr:nth-child(2) > td:nth-child(2)',
                usd_sell: '#data2  table tr:nth-child(2) > td:nth-child(3)',
                eur_buy: '#data2  table tr:nth-child(3) > td:nth-child(2)',
                eur_sell: '#data2  table tr:nth-child(3) > td:nth-child(3)',
                rub_buy: '#data2  table tr:nth-child(4) > td:nth-child(2)',
                rub_sell: '#data2  table tr:nth-child(4) > td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'alfabank.kz',
            url: 'http://www.alfabank.kz/',
            selectors: {
                usd_buy: '#div_tbl_kurs  table  tr:nth-child(3) > td:nth-child(2)',
                usd_sell: '#div_tbl_kurs  table  tr:nth-child(3) > td:nth-child(3)',
                eur_buy: '#div_tbl_kurs  table  tr:nth-child(4) > td:nth-child(2)',
                eur_sell: '#div_tbl_kurs  table  tr:nth-child(4) > td:nth-child(3)',
                rub_buy: '#div_tbl_kurs  table  tr:nth-child(2) > td:nth-child(2)',
                rub_sell: '#div_tbl_kurs  table  tr:nth-child(2) > td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'eximbank.kz',
            url: 'http://www.eximbank.kz/kursy_valyuty/kursy_valyut_v_obmennyh_punktah_',
            selectors: {
                usd_buy: 'table.tg  tr:nth-child(4) td:nth-child(2)',
                usd_sell: 'table.tg  tr:nth-child(4) td:nth-child(3)',
                eur_buy: 'table.tg  tr:nth-child(4) td:nth-child(4)',
                eur_sell: 'table.tg  tr:nth-child(4) td:nth-child(5)',
                rub_buy: 'table.tg  tr:nth-child(4) td:nth-child(6)',
                rub_sell: 'table.tg  tr:nth-child(4) td:nth-child(7)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'boc.kz',
            url: 'http://boc.kz/ru/',
            selectors: {
                usd_buy: 'table.courses  tr:nth-child(2) td:nth-child(2)',
                usd_sell: 'table.courses  tr:nth-child(2) td:nth-child(3)',
                eur_buy: 'table.courses  tr:nth-child(3) td:nth-child(2)',
                eur_sell: 'table.courses  tr:nth-child(3) td:nth-child(3)',
                rub_buy: 'table.courses_yur  tr:nth-child(4)  td:nth-child(2)',
                rub_sell: 'table.courses_yur  tr:nth-child(4)  td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'vtb-bank.kz',
            url: 'http://vtb-bank.kz/',
            selectors: {
                usd_buy: '#content table tr:nth-child(1) td:nth-child(2) span',
                usd_sell: '#content table tr:nth-child(1) td:nth-child(3) span',
                eur_buy: '#content table tr:nth-child(2) td:nth-child(2) span',
                eur_sell: '#content table tr:nth-child(2) td:nth-child(3) span',
                rub_buy: '#content table tr:nth-child(3) td:nth-child(2) span',
                rub_sell: '#content table tr:nth-child(3) td:nth-child(3) span'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'asiacreditbank.kz',
            url: 'http://www.asiacreditbank.kz/',
            selectors: {
                usd_buy: '#exchange-11 tr:nth-child(2) > td.cl-2',
                usd_sell: '#exchange-11 tr:nth-child(2) > td.cl-4',
                eur_buy: '#exchange-11 tr:nth-child(3) > td.cl-2',
                eur_sell: '#exchange-11 tr:nth-child(3) > td.cl-4',
                rub_buy: '#exchange-11 tr:nth-child(4) > td.cl-2',
                rub_sell: '#exchange-11 tr:nth-child(4) > td.cl-4'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'bankpozitiv.kz',
            url: 'https://www.bankpozitiv.kz/product/transactions/EXCHRATES_01.jsp',
            selectors: {
                usd_buy: '',
                usd_sell: 'table tr:nth-child(3) td table tr td:nth-child(3) table.table_borderColor tr td table tr:nth-child(2) td:nth-child(3)',
                eur_buy: 'table tr:nth-child(3) td table tr td:nth-child(3) table.table_borderColor tr td table tr:nth-child(3) td:nth-child(2)',
                eur_sell: 'table tr:nth-child(3) td table tr td:nth-child(3) table.table_borderColor tr td table tr:nth-child(3) td:nth-child(3)',
                rub_buy: 'table tr:nth-child(3) td table tr td:nth-child(3) table.table_borderColor tr td table tr:nth-child(5) td:nth-child(2)',
                rub_sell: 'table tr:nth-child(3) td table tr td:nth-child(3) table.table_borderColor tr td table tr:nth-child(5) td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'kassanova.kz',
            url: 'http://www.kassanova.kz/ru/currency',
            selectors: {
                usd_buy: '#currency-content  table > tbody > tr:nth-child(2) > td:nth-child(2)',
                usd_sell: '#currency-content table  tbody  tr:nth-child(2)  td:nth-child(3)',
                eur_buy: '#currency-content table  tbody  tr:nth-child(3)  td:nth-child(2)',
                eur_sell: '#currency-content table  tbody  tr:nth-child(3)  td:nth-child(3)',
                rub_buy: '#currency-content table  tbody  tr:nth-child(4)  td:nth-child(2)',
                rub_sell: '#currency-content table  tbody  tr:nth-child(4)  td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'bankrbk.kz',
            url: 'https://www.bankrbk.kz/rus',
            selectors: {
                usd_buy: 'tr.currency_tr:nth-child(3) > td:nth-child(2)',
                usd_sell: 'tr.currency_tr:nth-child(3) > td:nth-child(4)',
                eur_buy: 'tr.currency_tr:nth-child(4) > td:nth-child(2)',
                eur_sell: 'tr.currency_tr:nth-child(4) > td:nth-child(4)',
                rub_buy: 'tr.currency_tr:nth-child(2) > td:nth-child(2)',
                rub_sell: 'tr.currency_tr:nth-child(2) > td:nth-child(4)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'shinhan.kz',
            url: 'http://www.shinhan.kz/ru/main',
            selectors: {
                usd_buy: '#obmen_valut div.Courses div.valuta_table table:nth-child(1) tr:nth-child(2) > td:nth-child(2)',
                usd_sell: '#obmen_valut div.Courses div.valuta_table table:nth-child(1) tr:nth-child(2) > td:nth-child(3)',
                eur_buy: '#obmen_valut div.Courses div.valuta_table table:nth-child(1) tr:nth-child(3) > td:nth-child(2)',
                eur_sell: '#obmen_valut div.Courses div.valuta_table table:nth-child(1) tr:nth-child(3) > td:nth-child(3)',
                rub_buy: '#obmen_valut div.Courses div.valuta_table table:nth-child(1) tr:nth-child(4) > td:nth-child(2)',
                rub_sell: '#obmen_valut div.Courses div.valuta_table table:nth-child(1) tr:nth-child(4) > td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'capitalbank.kz',
            url: 'http://capitalbank.kz/oldsite/',
            selectors: {
                usd_buy: '#kurs > div:nth-child(4) > div.pos2',
                usd_sell: '#kurs > div:nth-child(4) > div.pos3',
                eur_buy: '#kurs > div:nth-child(5) > div.pos2',
                eur_sell: '#kurs > div:nth-child(5) > div.pos3',
                rub_buy: '',
                rub_sell: ''
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'qazaqbanki.kz',
            url: 'http://qazaqbanki.kz/kaz/',
            selectors: {
                usd_buy: '#currency-41  tr:nth-child(3) > td:nth-child(2)',
                usd_sell: '#currency-41  tr:nth-child(3) > td:nth-child(3)',
                eur_buy: '#currency-41  tr:nth-child(4) > td:nth-child(2)',
                eur_sell: '#currency-41  tr:nth-child(4) > td:nth-child(3)',
                rub_buy: '#currency-41  tr:nth-child(2) > td:nth-child(2)',
                rub_sell: '#currency-41  tr:nth-child(2) > td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'zamanbank.kz',
            url: 'http://zamanbank.kz/',
            selectors: {
                usd_buy: '#kurs > div:nth-child(2) > span:nth-child(2)',
                usd_sell: '#kurs > div:nth-child(2) > span:nth-child(3)',
                eur_buy: '#kurs > div:nth-child(3) > span:nth-child(2)',
                eur_sell: '#kurs > div:nth-child(3) > span:nth-child(3)',
                rub_buy: '#kurs > div:nth-child(4) > span:nth-child(2)',
                rub_sell: '#kurs > div:nth-child(4) > span:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'sberbank.kz',
            url: 'http://www.sberbank.kz/ru/individuals/rates/currencies',
            selectors: {
                usd_buy: '#main div.sbrf-div-list-inner.bp-area.row.body-container  div.layout.layout1 div.tabs-content  div.tabs-content-item.tab-content.tabs-content-item--state-opened  table  tbody  tr:nth-child(1) > td:nth-child(3)',
                usd_sell: '#main div.sbrf-div-list-inner.bp-area.row.body-container  div.layout.layout1 div.tabs-content  div.tabs-content-item.tab-content.tabs-content-item--state-opened  table  tbody  tr:nth-child(1) > td:nth-child(4)',
                eur_buy: '#main div.sbrf-div-list-inner.bp-area.row.body-container  div.layout.layout1 div.tabs-content  div.tabs-content-item.tab-content.tabs-content-item--state-opened  table  tbody  tr:nth-child(2) > td:nth-child(3)',
                eur_sell: '#main div.sbrf-div-list-inner.bp-area.row.body-container  div.layout.layout1 div.tabs-content  div.tabs-content-item.tab-content.tabs-content-item--state-opened  table  tbody  tr:nth-child(2) > td:nth-child(4)',
                rub_buy: '#main div.sbrf-div-list-inner.bp-area.row.body-container  div.layout.layout1 div.tabs-content  div.tabs-content-item.tab-content.tabs-content-item--state-opened  table  tbody  tr:nth-child(4) > td:nth-child(3)',
                rub_sell: '#main div.sbrf-div-list-inner.bp-area.row.body-container  div.layout.layout1 div.tabs-content  div.tabs-content-item.tab-content.tabs-content-item--state-opened  table  tbody  tr:nth-child(4) > td:nth-child(4)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }
];