'use strict';

var parsing = require('../parsing.js');

module.exports = [
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'fkb.kg',
            url: 'http://www.fkb.kg',
            selectors: {
                usd_buy: 'div.curency table tr:nth-child(2) > td:nth-child(2)',
                usd_sell: 'div.curency table tr:nth-child(2) > td:nth-child(3)',
                eur_buy: 'div.curency table tr:nth-child(3) > td:nth-child(2)',
                eur_sell: 'div.curency table tr:nth-child(3) > td:nth-child(3)',
                kzt_buy: 'div.curency table tr:nth-child(4) > td:nth-child(2)',
                kzt_sell: 'div.curency table tr:nth-child(4) > td:nth-child(3)',
                rub_buy: 'div.curency table tr:nth-child(5) > td:nth-child(2)',
                rub_sell: 'div.curency table tr:nth-child(5) > td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'nbp.kg',
            url: 'http://nbp.kg/?lang=ru',
            selectors: {
                usd_buy: '.textwidget  table:nth-child(1)  tbody:nth-child(1)  tr:nth-child(2) > td:nth-child(3)',
                usd_sell: '.textwidget  table:nth-child(1)  tbody:nth-child(1)  tr:nth-child(2) > td:nth-child(4)',
                eur_buy: '.textwidget  table:nth-child(1)  tbody:nth-child(1)  tr:nth-child(3)  td:nth-child(3)',
                eur_sell: '.textwidget  table:nth-child(1)  tbody:nth-child(1)  tr:nth-child(3)  td:nth-child(4)',
                kzt_buy: '',
                kzt_sell: '',
                rub_buy: '.textwidget  table:nth-child(1)  tbody:nth-child(1)  tr:nth-child(4)  td:nth-child(3)',
                rub_sell: '.textwidget  table:nth-child(1)  tbody:nth-child(1)  tr:nth-child(4)  td:nth-child(4)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'btabank.kg',
            url: 'http://www.btabank.kg/ru/',
            selectors: {
                usd_buy: 'div.currency  div  table tr:nth-child(3)  td:nth-child(2)',
                usd_sell: 'div.currency  div  table tr:nth-child(3)  td:nth-child(3)',
                eur_buy: 'div.currency  div  table tr:nth-child(4)  td:nth-child(2)',
                eur_sell: 'div.currency  div  table tr:nth-child(4)  td:nth-child(3)',
                kzt_buy: 'div.currency  div  table tr:nth-child(6)  td:nth-child(2)',
                kzt_sell: 'div.currency  div  table tr:nth-child(6)  td:nth-child(3)',
                rub_buy: 'div.currency  div  table tr:nth-child(5)  td:nth-child(2)',
                rub_sell: 'div.currency  div  table tr:nth-child(5)  td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'demirbank.kg',
            url: 'http://www.demirbank.kg',
            selectors: {
                usd_buy: '#moneytable  tr:nth-child(2) > td:nth-child(2)',
                usd_sell: '#moneytable  tr:nth-child(2) > td:nth-child(3)',
                eur_buy: '#moneytable  tr:nth-child(3) > td:nth-child(2)',
                eur_sell: '#moneytable  tr:nth-child(3) > td:nth-child(3)',
                kzt_buy: '#moneytable  tr:nth-child(5) > td:nth-child(2)',
                kzt_sell: '#moneytable  tr:nth-child(5) > td:nth-child(3)',
                rub_buy: '#moneytable  tr:nth-child(4) > td:nth-child(2)',
                rub_sell: '#moneytable  tr:nth-child(4) > td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'baitushum.kg',
            url: 'http://www.baitushum.kg/ru/',
            selectors: {
                usd_buy: '#rates-widget li.rate.usd > span.buy',
                usd_sell: '#rates-widget li.rate.usd > span.sell',
                eur_buy: '#rates-widget li.rate.eur > span.buy',
                eur_sell: '#rates-widget li.rate.eur > span.sell',
                kzt_buy: '#rates-widget li.rate.kzt > span.buy',
                kzt_sell: '#rates-widget li.rate.kzt > span.sell',
                rub_buy: '#rates-widget li.rate.rub > span.buy',
                rub_sell: '#rates-widget li.rate.rub > span.sell'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'bankasia.kg',
            url: 'http://www.bankasia.kg/',
            selectors: {
                usd_buy: '#USD',
                usd_sell: '#ja-col1  div:nth-child(2)  table  tr:nth-child(2)  td:nth-child(4)',
                eur_buy: '#EUR',
                eur_sell: '#ja-col1  div:nth-child(2)  table  tr:nth-child(3)  td:nth-child(4)',
                kzt_buy: '#KZT',
                kzt_sell: '#ja-col1  div:nth-child(2)  table  tr:nth-child(5)  td:nth-child(4)',
                rub_buy: '#RUB',
                rub_sell: '#ja-col1  div:nth-child(2)  table  tr:nth-child(4)  td:nth-child(4)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'kicb.net',
            url: 'http://www.kicb.net/welcome/',
            selectors: {
                usd_buy: '#curency > div:nth-child(2) > div:nth-child(3) > div.data2 > span',
                usd_sell: '#curency > div:nth-child(2) > div:nth-child(3) > div.data3 > span',
                eur_buy: '#curency > div:nth-child(2) > div:nth-child(4) > div.data2 > span',
                eur_sell: '#curency > div:nth-child(2) > div:nth-child(4) > div.data3 > span',
                kzt_buy: '#curency > div:nth-child(2) > div:nth-child(6) > div.data2 > span',
                kzt_sell: '#curency > div:nth-child(2) > div:nth-child(6) > div.data3 > span',
                rub_buy: '#curency > div:nth-child(2) > div:nth-child(5) > div.data2 > span',
                rub_sell: '#curency > div:nth-child(2) > div:nth-child(5) > div.data3 > span'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'tolubaybank.kg',
            url: 'http://www.tolubaybank.kg/',
            selectors: {
                usd_buy: '#tabs-1  table tr:nth-child(2) > td:nth-child(2)',
                usd_sell: '#tabs-1 table tr:nth-child(2) > td:nth-child(3)',
                eur_buy: '#tabs-1  table tr:nth-child(3) > td:nth-child(2)',
                eur_sell: '#tabs-1  table tr:nth-child(3) > td:nth-child(3)',
                kzt_buy: '#tabs-1  table tr:nth-child(5) > td:nth-child(2)',
                kzt_sell: '#tabs-1  table tr:nth-child(5) > td:nth-child(3)',
                rub_buy: '#tabs-1  table tr:nth-child(4) > td:nth-child(2)',
                rub_sell: '#tabs-1  table tr:nth-child(4) > td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'ab.kg',
            url: 'http://www.ab.kg/ru',
            selectors: {
                usd_buy: '#tab1  table tr:nth-child(2) > td:nth-child(2)',
                usd_sell: '#tab1  table tr:nth-child(2) > td:nth-child(3)',
                eur_buy: '#tab1  table tr:nth-child(3) > td:nth-child(2)',
                eur_sell: '#tab1  table tr:nth-child(3) > td:nth-child(3)',
                kzt_buy: '#tab1  table tr:nth-child(4) > td:nth-child(2)',
                kzt_sell: '#tab1  table tr:nth-child(4) > td:nth-child(3)',
                rub_buy: '#tab1  table tr:nth-child(5) > td:nth-child(2)',
                rub_sell: '#tab1  table tr:nth-child(5) > td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'capitalbank.kg',
            url: 'http://www.capitalbank.kg/',
            selectors: {
                usd_buy: 'div.content  table:nth-child(1)  tr:nth-child(2) > td:nth-child(3)',
                usd_sell: 'div.content  table:nth-child(1)  tr:nth-child(2) > td:nth-child(5)',
                eur_buy: 'div.content table:nth-child(1)  tr:nth-child(3) > td:nth-child(3)',
                eur_sell: 'div.content table:nth-child(1)  tr:nth-child(3) > td:nth-child(5)',
                kzt_buy: 'div.content table:nth-child(1)  tr:nth-child(5) > td:nth-child(3)',
                kzt_sell: 'div.content  table:nth-child(1)  tr:nth-child(5) > td:nth-child(5)',
                rub_buy: 'div.content table:nth-child(1)  tr:nth-child(4) > td:nth-child(3)',
                rub_sell: 'div.content  table:nth-child(1)  tr:nth-child(4) > td:nth-child(5)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'bakai.kg',
            url: 'http://www.bakai.kg/',
            selectors: {
                usd_buy: '#accordion table tr:nth-child(2) > td:nth-child(2)',
                usd_sell: '#accordion table tr:nth-child(2) > td:nth-child(3)',
                eur_buy: '#accordion table tr:nth-child(3) > td:nth-child(2)',
                eur_sell: '#accordion table tr:nth-child(3) > td:nth-child(3)',
                kzt_buy: '#accordion table tr:nth-child(5) > td:nth-child(2)',
                kzt_sell: '#accordion table tr:nth-child(5) > td:nth-child(3)',
                rub_buy: '#accordion table tr:nth-child(4) > td:nth-child(2)',
                rub_sell: '#accordion table tr:nth-child(4) > td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'doscredobank.kg',
            url: 'http://www.doscredobank.kg/',
            selectors: {
                usd_buy: '#block-fexchange-rates > div > div.front-exchange-block.front-blck > div > div.rate-wrapper > table:nth-child(1) > tbody > tr:nth-child(1) > td:nth-child(2)',
                usd_sell: '#block-fexchange-rates > div > div.front-exchange-block.front-blck > div > div.rate-wrapper > table:nth-child(1) > tbody > tr:nth-child(1) > td:nth-child(3)',
                eur_buy: '#block-fexchange-rates > div > div.front-exchange-block.front-blck > div > div.rate-wrapper > table:nth-child(1) > tbody > tr:nth-child(2) > td:nth-child(2)',
                eur_sell: '#block-fexchange-rates > div > div.front-exchange-block.front-blck > div > div.rate-wrapper > table:nth-child(1) > tbody > tr:nth-child(2) > td:nth-child(3)',
                kzt_buy: '#block-fexchange-rates > div > div.front-exchange-block.front-blck > div > div.rate-wrapper > table:nth-child(1) > tbody > tr:nth-child(4) > td:nth-child(2)',
                kzt_sell: '#block-fexchange-rates > div > div.front-exchange-block.front-blck > div > div.rate-wrapper > table:nth-child(1) > tbody > tr:nth-child(4) > td:nth-child(3)',
                rub_buy: '#block-fexchange-rates > div > div.front-exchange-block.front-blck > div > div.rate-wrapper > table:nth-child(1) > tbody > tr:nth-child(3) > td:nth-child(2)',
                rub_sell: '#block-fexchange-rates > div > div.front-exchange-block.front-blck > div > div.rate-wrapper > table:nth-child(1) > tbody > tr:nth-child(3) > td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'cbk.kg',
            url: 'http://www.cbk.kg/',
            selectors: {
                usd_buy: '#rates-table > table > tr.usd > td.buy',
                usd_sell: '#rates-table > table > tr.usd > td.sell',
                eur_buy: '#rates-table > table > tr.euro > td.buy',
                eur_sell: '#rates-table > table > tr.euro > td.sell',
                kzt_buy: '#rates-table > table > tr.kzt > td.buy',
                kzt_sell: '#rates-table > table > tr.kzt > td.sell',
                rub_buy: '#rates-table > table > tr.rub > td.buy',
                rub_sell: '#rates-table > table > tr.rub > td.sell'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'kcredit.kg',
            url: 'http://www.kcredit.kg',
            selectors: {
                usd_buy: '#art-main > div.art-sheet > div.art-sheet-body > div.art-content-layout > div > div.art-layout-cell.art-sidebar2 > div:nth-child(2) > div.art-block-body > div.art-blockcontent > div > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(2)',
                usd_sell: '#art-main > div.art-sheet > div.art-sheet-body > div.art-content-layout > div > div.art-layout-cell.art-sidebar2 > div:nth-child(2) > div.art-block-body > div.art-blockcontent > div > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(3)',
                eur_buy: '#art-main > div.art-sheet > div.art-sheet-body > div.art-content-layout > div > div.art-layout-cell.art-sidebar2 > div:nth-child(2) > div.art-block-body > div.art-blockcontent > div > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(2)',
                eur_sell: '#art-main > div.art-sheet > div.art-sheet-body > div.art-content-layout > div > div.art-layout-cell.art-sidebar2 > div:nth-child(2) > div.art-block-body > div.art-blockcontent > div > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(3)',
                kzt_buy: '#art-main > div.art-sheet > div.art-sheet-body > div.art-content-layout > div > div.art-layout-cell.art-sidebar2 > div:nth-child(2) > div.art-block-body > div.art-blockcontent > div > table:nth-child(2) > tbody > tr:nth-child(5) > td:nth-child(2)',
                kzt_sell: '#art-main > div.art-sheet > div.art-sheet-body > div.art-content-layout > div > div.art-layout-cell.art-sidebar2 > div:nth-child(2) > div.art-block-body > div.art-blockcontent > div > table:nth-child(2) > tbody > tr:nth-child(5) > td:nth-child(3)',
                rub_buy: '#art-main > div.art-sheet > div.art-sheet-body > div.art-content-layout > div > div.art-layout-cell.art-sidebar2 > div:nth-child(2) > div.art-block-body > div.art-blockcontent > div > table:nth-child(2) > tbody > tr:nth-child(4) > td:nth-child(2)',
                rub_sell: '#art-main > div.art-sheet > div.art-sheet-body > div.art-content-layout > div > div.art-layout-cell.art-sidebar2 > div:nth-child(2) > div.art-block-body > div.art-blockcontent > div > table:nth-child(2) > tbody > tr:nth-child(4) > td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'rsk.kg',
            url: 'http://www.rsk.kg/',
            selectors: {
                usd_buy: '#page-content > div:nth-child(1) > div.photo-block.exchange > div.course-block > div.course-item.active > div.course-list > div:nth-child(4) > div:nth-child(2)',
                usd_sell: '#page-content > div:nth-child(1) > div.photo-block.exchange > div.course-block > div.course-item.active > div.course-list > div:nth-child(4) > div:nth-child(3)',
                eur_buy: '#page-content > div:nth-child(1) > div.photo-block.exchange > div.course-block > div.course-item.active > div.course-list > div:nth-child(6) > div:nth-child(2)',
                eur_sell: '#page-content > div:nth-child(1) > div.photo-block.exchange > div.course-block > div.course-item.active > div.course-list > div:nth-child(6) > div:nth-child(3)',
                kzt_buy: '#page-content > div:nth-child(1) > div.photo-block.exchange > div.course-block > div.course-item.active > div.course-list > div:nth-child(7) > div:nth-child(2)',
                kzt_sell: '#page-content > div:nth-child(1) > div.photo-block.exchange > div.course-block > div.course-item.active > div.course-list > div:nth-child(7) > div:nth-child(3)',
                rub_buy: '#page-content > div:nth-child(1) > div.photo-block.exchange > div.course-block > div.course-item.active > div.course-list > div:nth-child(5) > div:nth-child(2)',
                rub_sell: '#page-content > div:nth-child(1) > div.photo-block.exchange > div.course-block > div.course-item.active > div.course-list > div:nth-child(5) > div:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'halykbank.kg',
            url: 'http://www.halykbank.kg/',
            selectors: {
                usd_buy: 'body > table:nth-child(4)  tr:nth-child(1) > td:nth-child(2) > table:nth-child(3)  > tr:nth-child(2) > td:nth-child(2)',
                usd_sell: 'body > table:nth-child(4)  tr:nth-child(1) > td:nth-child(2) > table:nth-child(3) > tr:nth-child(2) > td:nth-child(5)',
                eur_buy: 'body > table:nth-child(4)  tr:nth-child(1) > td:nth-child(2) > table:nth-child(3) > tr:nth-child(3) > td:nth-child(2)',
                eur_sell: 'body > table:nth-child(4)  tr:nth-child(1) > td:nth-child(2) > table:nth-child(3)  > tr:nth-child(3) > td:nth-child(5)',
                kzt_buy: 'body > table:nth-child(4)  tr:nth-child(1) > td:nth-child(2) > table:nth-child(3)  > tr:nth-child(5) > td:nth-child(2)',
                kzt_sell: 'body > table:nth-child(4)  tr:nth-child(1) > td:nth-child(2) > table:nth-child(3)  > tr:nth-child(5) > td:nth-child(5)',
                rub_buy: 'body > table:nth-child(4)  tr:nth-child(1) > td:nth-child(2) > table:nth-child(3)  > tr:nth-child(4) > td:nth-child(2)',
                rub_sell: 'body > table:nth-child(4)  tr:nth-child(1) > td:nth-child(2) > table:nth-child(3)  > tr:nth-child(4) > td:nth-child(5)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'ecoislamicbank.kg',
            url: 'http://ecoislamicbank.kg/',
            selectors: {
                usd_buy: 'div.kursval div.table div:nth-child(2) > span:nth-child(2)',
                usd_sell: 'div.kursval div.table div:nth-child(2) > span:nth-child(3)',
                eur_buy: 'div.kursval div.table div:nth-child(3) > span:nth-child(2)',
                eur_sell: 'div.kursval div.table div:nth-child(3) > span:nth-child(3)',
                kzt_buy: 'div.kursval div.table div:nth-child(5) > span:nth-child(2)',
                kzt_sell: 'div.kursval div.table div:nth-child(5) > span:nth-child(3)',
                rub_buy: 'div.kursval div.table div:nth-child(4) > span:nth-child(2)',
                rub_sell: 'div.kursval div.table div:nth-child(4) > span:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'optimabank.kg',
            url: 'http://www.optimabank.kg/en/',
            selectors: {
                usd_buy: '#mod_nbrates > div:nth-child(2) > div > table > tbody > tr:nth-child(1) > td:nth-child(2) > span',
                usd_sell: '#mod_nbrates > div:nth-child(2) > div > table > tbody > tr:nth-child(1) > td:nth-child(3) > span',
                eur_buy: '#mod_nbrates > div:nth-child(2) > div > table > tbody > tr:nth-child(2) > td:nth-child(2) > span',
                eur_sell: '#mod_nbrates > div:nth-child(2) > div > table > tbody > tr:nth-child(2) > td:nth-child(3) > span',
                kzt_buy: '#mod_nbrates > div:nth-child(2) > div > table > tbody > tr:nth-child(4) > td:nth-child(2) > span',
                kzt_sell: '#mod_nbrates > div:nth-child(2) > div > table > tbody > tr:nth-child(4) > td:nth-child(3) > span',
                rub_buy: '#mod_nbrates > div:nth-child(2) > div > table > tbody > tr:nth-child(3) > td:nth-child(2) > span',
                rub_sell: '#mod_nbrates > div:nth-child(2) > div > table > tbody > tr:nth-child(3) > td:nth-child(3) > span'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'amanbank.kg',
            url: 'http://www.amanbank.kg/',
            selectors: {
                usd_buy: '#art-main > div > div.art-content-layout > div > div:nth-child(1) > div > div > div > div > div > div.supertable-col.supertable-col-even.supertable-col-2 > div.supertable-cell.supertable-row-even.supertable-row-2 > div > div',
                usd_sell: '#art-main > div > div.art-content-layout > div > div:nth-child(1) > div > div > div > div > div > div.supertable-col.supertable-col-last.supertable-col-odd.supertable-col-3 > div.supertable-cell.supertable-row-even.supertable-row-2 > div > div',
                eur_buy: '#art-main > div > div.art-content-layout > div > div:nth-child(1) > div > div > div > div > div > div.supertable-col.supertable-col-even.supertable-col-2 > div.supertable-cell.supertable-row-odd.supertable-row-3 > div > div',
                eur_sell: '#art-main > div > div.art-content-layout > div > div:nth-child(1) > div > div > div > div > div > div.supertable-col.supertable-col-last.supertable-col-odd.supertable-col-3 > div.supertable-cell.supertable-row-odd.supertable-row-3 > div > div',
                kzt_buy: '#art-main > div > div.art-content-layout > div > div:nth-child(1) > div > div > div > div > div > div.supertable-col.supertable-col-even.supertable-col-2 > div.supertable-cell.supertable-row-last.supertable-row-odd.supertable-row-5 > div > div',
                kzt_sell: '#art-main > div > div.art-content-layout > div > div:nth-child(1) > div > div > div > div > div > div.supertable-col.supertable-col-last.supertable-col-odd.supertable-col-3 > div.supertable-cell.supertable-row-last.supertable-row-odd.supertable-row-5 > div > div',
                rub_buy: '#art-main > div > div.art-content-layout > div > div:nth-child(1) > div > div > div > div > div > div.supertable-col.supertable-col-even.supertable-col-2 > div.supertable-cell.supertable-row-even.supertable-row-4 > div > div',
                rub_sell: '#art-main > div > div.art-content-layout > div > div:nth-child(1) > div > div > div > div > div > div.supertable-col.supertable-col-last.supertable-col-odd.supertable-col-3 > div.supertable-cell.supertable-row-even.supertable-row-4 > div > div'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    },
    {
        startFunc: parsing.loadPage,
        args: {
            id: 'rib.kg',
            url: 'http://www.rib.kg/',
            selectors: {
                usd_buy: '#sub-body > div > div.wrapper > div.content > div.three-coll > div.right-coll > div.for-currency > div > table > tbody > tr:nth-child(1) > td:nth-child(2)',
                usd_sell: '#sub-body > div > div.wrapper > div.content > div.three-coll > div.right-coll > div.for-currency > div > table > tbody > tr:nth-child(1) > td.last',
                eur_buy: '#sub-body > div > div.wrapper > div.content > div.three-coll > div.right-coll > div.for-currency > div > table > tbody > tr:nth-child(2) > td:nth-child(2)',
                eur_sell: '#sub-body > div > div.wrapper > div.content > div.three-coll > div.right-coll > div.for-currency > div > table > tbody > tr:nth-child(2) > td.last',
                kzt_buy: '#sub-body > div > div.wrapper > div.content > div.three-coll > div.right-coll > div.for-currency > div > table > tbody > tr:nth-child(3) > td:nth-child(2)',
                kzt_sell: '#sub-body > div > div.wrapper > div.content > div.three-coll > div.right-coll > div.for-currency > div > table > tbody > tr:nth-child(3) > td.last',
                rub_buy: '#sub-body > div > div.wrapper > div.content > div.three-coll > div.right-coll > div.for-currency > div > table > tbody > tr:nth-child(4) > td:nth-child(2)',
                rub_sell: '#sub-body > div > div.wrapper > div.content > div.three-coll > div.right-coll > div.for-currency > div > table > tbody > tr:nth-child(4) > td.last'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'ksbc.kg',
            url: 'http://www.ksbc.kg/',
            selectors: {
                usd_buy: '#boxed-area > div:nth-child(3) > div:nth-child(1) > div > div > div > table > tbody > tr:nth-child(1) > td:nth-child(2)',
                usd_sell: '#boxed-area > div:nth-child(3) > div:nth-child(1) > div > div > div > table > tbody > tr:nth-child(1) > td:nth-child(3)',
                eur_buy: '#boxed-area > div:nth-child(3) > div:nth-child(1) > div > div > div > table > tbody > tr:nth-child(2) > td:nth-child(2)',
                eur_sell: '#boxed-area > div:nth-child(3) > div:nth-child(1) > div > div > div > table > tbody > tr:nth-child(2) > td:nth-child(3)',
                kzt_buy: '#boxed-area > div:nth-child(3) > div:nth-child(1) > div > div > div > table > tbody > tr:nth-child(4) > td:nth-child(2)',
                kzt_sell: '#boxed-area > div:nth-child(3) > div:nth-child(1) > div > div > div > table > tbody > tr:nth-child(4) > td:nth-child(3)',
                rub_buy: '#boxed-area > div:nth-child(3) > div:nth-child(1) > div > div > div > table > tbody > tr:nth-child(3) > td:nth-child(2)',
                rub_sell: '#boxed-area > div:nth-child(3) > div:nth-child(1) > div > div > div > table > tbody > tr:nth-child(3) > td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }, {
        startFunc: parsing.loadPage,
        args: {
            id: 'finca.kg',
            url: 'http://www.finca.kg/',
            selectors: {
                usd_buy: '#rates-1  table  tbody  tr:nth-child(1) > td:nth-child(2)',
                usd_sell: '#rates-1  table tbody tr:nth-child(1) > td:nth-child(3)',
                eur_buy: '#rates-1  table tbody tr:nth-child(2) > td:nth-child(2)',
                eur_sell: '#rates-1  table tbody tr:nth-child(2) > td:nth-child(3)',
                kzt_buy: '#rates-1  table  tbody  tr:nth-child(4) > td:nth-child(2)',
                kzt_sell: '#rates-1  table tbody tr:nth-child(4) > td:nth-child(3)',
                rub_buy: '#rates-1  table tbody tr:nth-child(3) > td:nth-child(2)',
                rub_sell: '#rates-1  table tbody tr:nth-child(3) > td:nth-child(3)'
            },
            lib: parsing.cheerio,
            valuesGetter: parsing.getByCssSelectors
        }
    }
];