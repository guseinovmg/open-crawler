'use strict';

const config = require('./config');
const utils = require('./utils.js');

if (config.debug_mode) {
    let banks = require(`./countries/${process.argv[3]}.js`);

    for (let bank of banks) {
        utils.deepFreeze(bank);
    }

    let debug_bank = process.argv[4];

    let bank;

    if (debug_bank === 'last') {
        bank = banks.slice(-1)[0];
    } else {
        bank = banks.filter(e =>  e.args.id === debug_bank)[0];
    }
    if (!bank){
        throw `bank ${debug_bank} not found in country ${process.argv[3]}`;
    }
    bank.startFunc(bank.args);
    return;
}

console.log('config.period ', (config.period / 1000).toFixed(3), ' sec');
console.log('config.countries ', config.countries);
console.log('config.data_queue_length ', config.data_queue_length);

const countries = [];

for (let country of config.countries) {
    countries[country] = require(`./countries/${country}.js`);
    //quick tests
    for (let bank of countries[country]) {
        if (!bank.args) throw `field 'args' must be exists in bank ${JSON.stringify(bank, null, 2)} (country ${country})`;
        if (!bank.args.id) throw `field 'args.id' must be exists in bank ${JSON.stringify(bank, null, 2)} (country ${country})`;
        if (!bank.startFunc) throw `field 'startFunc' must be exists in bank ${bank.args.id} (country ${country})`;
        if (bank.args.country) throw `field 'args.country' already exists in bank ${bank.args.id} (country ${country})`;
        if (Object.getOwnPropertyNames(bank).length !== 2) throw `unusual properties in bank ${bank.args.id} (country ${country})`;
        bank.args.country = country;
    }
}

let all_banks = [];

for (let country in countries) {
    all_banks = all_banks.concat(countries[country]);
}

utils.deepFreeze(all_banks);

require('./server.js'); // launches server;

let i = 0;

function getData() {
    let bank = all_banks[i];
    i++;
    if (i === all_banks.length) i = 0;
    bank.startFunc(bank.args);
}

let interval = Math.ceil(config.period / all_banks.length);

getData();

setInterval(getData, interval);

