'use strict';

function strToNumber(str) {
    if (str === null) return null;
    if (typeof str === 'string') {
        str = str.replace(/<!--[\w\W]*?-->/gi, '');
        str = str.replace(/<img[\w\W]*?>/gi, '');
        str = str.match(/\d+([.,]\d+)?/g);
        if (!str) return null;
        str = str[0].replace(/,/g, '.');
    }
    return Number(str);
}

function checkResponse(error, response) {
    if (!error) {
        if (response) {
            if (response.statusCode == 200) {
                return null;
            } else {
                return {response_code: response.statusCode};
            }
        } else {
            return {response: null};
        }
    } else {
        return {error: error};
    }
}

function stringsToNumbers(parser, rates) {
    if (parser)
        for (let c in rates) {
            if (rates[c])
                rates[c] = parser(rates[c]);
        }

    for (let c in rates) {
        rates[c] = strToNumber(rates[c]);
    }
}

function deepFreeze(obj) {
    var prop_names = Object.getOwnPropertyNames(obj);
    prop_names.forEach((name)=> {
        var prop = obj[name];
        if (typeof prop == 'object' && prop !== null && !Object.isFrozen(prop))
            deepFreeze(prop);
    });

    return Object.freeze(obj);
}

function checkSum(str) {
    var hash = 5380;
    var i = str.length;

    while (i)
        hash = (hash * 31) ^ str.charCodeAt(--i);

    hash = (hash * 7) ^ 254368944;
    /*https://github.com/darkskyapp/string-hash
     *JavaScript does bitwise operations (like XOR, above) on 32-bit signed
     * integers. Since we want the results to be always positive, convert the
     * signed int to an unsigned by doing an unsigned bitshift. */
    return hash >>> 0;
}

module.exports.checkSum = checkSum;

module.exports.checkResponse = checkResponse;

module.exports.strToNumber = strToNumber;

module.exports.stringsToNumbers = stringsToNumbers;

module.exports.deepFreeze = deepFreeze;


