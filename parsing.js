'use strict';

const request = require('request');
const cheerio = require('cheerio');
const jsdom = require('jsdom');

const profiler = require('./profiler.js');
const utils = require('./utils.js');
const config = require('./config.js');

let data_queue = [];

function onComplete(data) {
    if (config.debug_mode) {
        console.log(data);
        return;
    }
    data_queue.push(data);
    if (data_queue.length < config.data_queue_length) {
        return;
    }
    let time_start = Date.now();
    let data_queue_str = JSON.stringify(data_queue);
    let checksum = utils.checkSum(data_queue_str);
    data_queue = [];
    request.post({
            url: config.api_server_url,
            headers: {'User-Agent': config.user_agent},
            form: {data: data_queue_str, checksum: checksum}
        },
        (error, response) => {
            let duration = Date.now() - time_start;
            let decorated_error = utils.checkResponse(error, response);
            if (decorated_error) {
                profiler.addApiServerNetworkError(config.api_server_url, decorated_error);
                return;
            }
            profiler.addRequestDuration(config.api_server_url, duration);
        });
}

function loadPage(args) {
    let time_start = Date.now();
    request({'url': args.url, 'User-Agent': config.user_agent},
        (error, response, body) => {
            let duration = Date.now() - time_start;
            let decorated_error = utils.checkResponse(error, response);
            if (decorated_error) {
                profiler.addNetworkError(args.url, decorated_error);
                return;
            }

            profiler.addRequestDuration(args.url, duration);

            let parsing_start = Date.now();
            let result = args.valuesGetter(args, body);
            profiler.addParsingDuration(args.url, Date.now() - parsing_start);
            onComplete(result);
        });
}

function getByCssSelectors(args, page) {
    let result = {};
    result.id = args.id;
    result.rates = {};
    let $ = args.lib.load(page);
    for (let selector in args.selectors) {
        result.rates[selector] = $(args.selectors[selector]).html();
    }
    utils.stringsToNumbers(args.parser, result.rates);
    return result;
}

module.exports.cheerio = cheerio;

module.exports.jsdom = jsdom;

module.exports.onComplete = onComplete;

module.exports.loadPage = loadPage;

module.exports.getByCssSelectors = getByCssSelectors;