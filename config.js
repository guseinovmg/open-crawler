'use strict';

const fs = require('fs');

const USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36';
const API_SERVER_URL = 'http://localhost:3000';
const PERIOD = 20000;
const DATA_QUEUE_LENGTH = 5;
const MIN_PERIOD = 5000;

let file, config, file_parsed;

try {
    file = fs.readFileSync('./config.json', 'utf-8');
} catch (error) {
    console.log(`config.json doesn't exist`);
}

if (file) {
    try {
        file_parsed = JSON.parse(file);
    } catch (error) {
        throw 'config.json is not valid json';
    }
    if (typeof file_parsed.api_server_url !== 'string')
        throw `in config.json must be parameter api_server_url, but ${file_parsed.api_server_url} found`;

    if (typeof file_parsed.period !== 'number')
        throw `in config.json must be parameter period, but ${file_parsed.period} found`;

    if (typeof file_parsed.data_queue_length !== 'number')
        throw `in config.json must be parameter data_queue_length, but ${file_parsed.data_queue_length} found`;

    if (typeof file_parsed.user_agent !== 'string')
        throw `in config.json must be parameter user_agent, but ${file_parsed.user_agent} found`;

    if (!Array.isArray(file_parsed.countries))
        throw `in config.json must be parameter countries, but ${file_parsed.countries} found`;

    if (file_parsed.period < MIN_PERIOD)
        throw `period in config.json equal ${file_parsed.period}, but min value is ${MIN_PERIOD}`;

    config = file_parsed;

} else {
    let countries = fs.readdirSync('./countries').map(e => e.replace('.js', ''));
    let new_config_json = {
        user_agent: USER_AGENT,
        api_server_url: API_SERVER_URL,
        period: PERIOD,
        data_queue_length: DATA_QUEUE_LENGTH,
        countries: countries
    };
    fs.writeFileSync('./config.json', JSON.stringify(new_config_json, null, 4));
    console.log('new config.json created');
    config = new_config_json;
}

config.debug_mode = (()=> {
    if (process.argv.length > 2) {
        if (process.argv.length !== 5) throw 'wrong number of arguments';
        if (process.argv[2] !== 'debug-bank') throw 'wrong arguments';
        console.log('debug mode');
        return true;
    }
    return false;
})();

Object.freeze(config);
Object.freeze(config.countries);

module.exports = config;