'use strict';

const profiler = require('./profiler.js');
const path = require('path');
const http = require('http');
const express = require('express');
const app = express();

const PORT = 5000;

app.set('port', PORT);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

http.createServer(app).listen(PORT, function () {
    console.log(`Start server on port ${PORT}`);
});

app.use('/stat', require('./routes/stat.js').router);

app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function (err, req, res) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
