'use strict';

const config = require('./config');

const api_server_network_errors = {};
const network_errors = {};
const request_durations = {};
const parsing_durations = {};

function getNewDurationObj(duration) {
    return {min: duration, avg: duration, max: duration, last: duration, count: 1, date: Date.now()};
}

function addDuration(prev_obj, new_duration) {
    prev_obj.min = Math.min(prev_obj.min, new_duration);
    prev_obj.avg = Math.round((prev_obj.avg * prev_obj.count + new_duration) / (prev_obj.count + 1));
    prev_obj.max = Math.max(prev_obj.max, new_duration);
    prev_obj.last = new_duration;
    prev_obj.count++;
    prev_obj.date = Date.now();
}

module.exports.addApiServerNetworkError = (url, decorated_error)=> {
    if (config.debug_mode) {
        console.log(`${url}  api_server_network_error ${JSON.stringify(decorated_error)}`);
        return;
    }
    if (!(url in api_server_network_errors))
        api_server_network_errors[url] = {};

    let decorated_error_str = JSON.stringify(decorated_error);
    if (!(decorated_error_str in api_server_network_errors))
        api_server_network_errors[url][decorated_error_str] = 0;

    api_server_network_errors[url][decorated_error_str]++;
};

module.exports.addNetworkError = (url, decorated_error)=> {
    if (config.debug_mode) {
        console.log(`${url}  network error ${JSON.stringify(decorated_error)}`);
        return;
    }
    if (!(url in network_errors))
        network_errors[url] = {};

    let decorated_error_str = JSON.stringify(decorated_error);
    if (!(decorated_error_str in network_errors[url]))
        network_errors[url][decorated_error_str] = [];

    network_errors[url][decorated_error_str].push(Date.now());
};

module.exports.addRequestDuration = (url, duration)=> {
    if (config.debug_mode) {
        console.log(`${url}  request duration ${duration}`);
        return;
    }
    if (!(url in request_durations)) {
        request_durations[url] = getNewDurationObj(duration);
        return;
    }

    addDuration(request_durations[url], duration);
};

module.exports.addParsingDuration = (url, duration)=> {
    if (config.debug_mode) {
        console.log(`${url}  parsing duration ${duration}`);
        return;
    }
    if (!(url in parsing_durations)) {
        parsing_durations[url] = getNewDurationObj(duration);
        return;
    }

    addDuration(parsing_durations[url], duration);
};

module.exports.getParsingDurations = ()=> {
    return parsing_durations;
};

module.exports.getRequestDurations = ()=> {
    return request_durations;
};

module.exports.getNetworkErrors = (url)=> {
    if (url) {
        if (network_errors[url]) {
            let result = {};
            result[url] = network_errors[url];
            return result;
        }
    } else {
        let result = {};
        for (let url in network_errors) {
            result[url] = {};
            for (let err in network_errors[url]) {
                result[url][err] = network_errors[url][err].length;
            }
        }
        return result;
    }
    return {};
};

module.exports.getApiServerNetworkErrors = ()=> {
    return api_server_network_errors;
};