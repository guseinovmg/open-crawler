'use strict';

const profiler = require('../profiler.js');
const express = require('express');
const router = express.Router();

router.get('/', function (req, res) {
    res.redirect('/stat/request-durations');
});

router.get('/network-errors/api-server', function (req, res) {
    res.render('network_errors', {data: 'var data =' + JSON.stringify(profiler.getApiServerNetworkErrors())});
});

router.get('/network-errors/:url', function (req, res) {
    res.render('network_errors_detailed', {data: 'var data =' + JSON.stringify(profiler.getNetworkErrors(req.params.url))});
});

router.get('/network-errors', function (req, res) {
    res.render('network_errors', {data: 'var data =' + JSON.stringify(profiler.getNetworkErrors())});
});

router.get('/parsing-durations', function (req, res) {
    res.render('durations', {data: 'var data =' + JSON.stringify(profiler.getParsingDurations())});
});

router.get('/request-durations', function (req, res) {
    res.render('durations', {data: 'var data =' + JSON.stringify(profiler.getRequestDurations())});
});

module.exports.router = router;